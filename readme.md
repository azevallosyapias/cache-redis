# Project Caching with Redis

_Proyecto Backend para el manejo de memoria Cache usando Redis e integrando Micrometer para las obtener las metricas del proyecto, para leerlas con Premetheus y mostrarlas con Grafana_


_Para poder utilizar el API, se adjunta el File exportado de POSTMAN y se encuentra en la siguiente ruta:_

```
code-challenge/code_challenge.postman_collection.json
```


_El portal brindado por Grafana nos permitirá observar el estado del MS en tiempo real así como también onstruir nuestros propios Dashboard_

## Comenzando 🚀

_Para obtener una copia del repositorio solo se necesitaría clonar con el siguiente comando: ._

```
git clone https://azevallosyapias@bitbucket.org/azevallosyapias/cache-redis.git
```

### Pre-requisitos 📋

_Para poder desplegar el proyecto es necesario tener instalado:_

```
- Maven
- Java version 8 o superior
- Docker

```

## Construido con 🛠️

_Herramientas utilizadas en el desarrollo del proyecto_

```
* Java 8
* Spring Framework
* Spring Boot
* Spring MVC
* Spring JPA
* Swagger: utilizado para la documentacion del contrato del API
* Jacoco: utilizado para la revision y visualizacion de la cobertura del codigo
* JavaDocs: utilizado para documentar el codigo
* BD PostgreSQL 9.5
* Docker
* Postman para las pruebas
* Para la visualización de las metricas se utilizaron:
	*	Micrometer
	*	Prometheus
	*	Grafana
```


## Autor ✒️

* **Marino Alonso Z.Y. **

_Links:_
* [Linkedin](https://www.linkedin.com/in/marino-alonso-zevallos-yapias-82a200196/)
* Bitbucket : [azevallosyapias](https://bitbucket.org/azevallosyapias/)
* GitHub: [alonsozy](https://github.com/alonsozy)
* DockerHub: [alonsozy](https://hub.docker.com/u/alonsozy)



