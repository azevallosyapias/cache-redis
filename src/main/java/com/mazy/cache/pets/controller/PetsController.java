package com.mazy.cache.pets.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mazy.cache.config.entity.Pets;
import com.mazy.cache.service.PetsService;

import io.micrometer.core.annotation.Timed;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("")
@Api(tags = "default")
public class PetsController {

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PetsService petsService;

	
	/**
	 * <p> Metodo para retornar la lista de Mascotas de la BD
	 * se creo la metrica de timeout para este endPoint con la anotacion @Timed
	 * </p>
	 * @author Alonso
	 * @param void
	 * @return La lista de Mascotas 
	 */
	@Timed("get.pets")
	@ApiOperation(value = "Get all Pets", notes = "Get all pets")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK") })
	@GetMapping(path = "/getPets", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public List<Pets> listPets() {
		log.info("GetMapping - value: /getPets");
		return petsService.findAllPets();
	}

	/**
	 * <p> Metodo para registrar una Mascota
	 * </p>
	 * @author Alonso
	 * @param Objeto Pet
	 * @return El usuario registrado
	 */
	@ApiOperation(value = "Create Pet", notes = "")
	@ApiResponses({
		@ApiResponse(code = HttpServletResponse.SC_CREATED, message ="CREATED"), 
		@ApiResponse(code = HttpServletResponse.SC_METHOD_NOT_ALLOWED, message ="Invalid input") 
	})
	@PostMapping(path = "/createPets", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public Pets createUser(@RequestBody Pets pet) {
		log.info("PostMapping - value: /createPets");
		return petsService.createPet(pet);
	}

	
	/**
	 * <p> Metodo para eliminar un Pet
	 * </p>
	 * @author Alonso
	 * @param petId : Id de la mascota
	 * @return 
	 */
	@ApiOperation(value = "", notes = "")
	@ApiResponses({ @ApiResponse(code = HttpServletResponse.SC_OK, message = "OK"), 
					@ApiResponse(code = HttpServletResponse.SC_BAD_REQUEST, message ="Invalid pet id"), 
					@ApiResponse(code = HttpServletResponse.SC_NOT_FOUND, message ="Pet not found") 
	})
	@DeleteMapping(path = "/deleteById/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public void deleteUser(@PathVariable("userId") Long userId) {
		log.info("DeleteMapping - value: /deleteUsersById/"+userId);
		petsService.deleteById(userId);
	}
	
}


