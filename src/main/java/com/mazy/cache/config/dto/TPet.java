package com.mazy.cache.config.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.mazy.cache.util.Constants.OBSERV;

@Entity
@Table(name = "t_pet")
public class TPet implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9190271193694838570L;

	@Id
	@SequenceGenerator(name = "sequence_pet", sequenceName = "sq_pets", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_pet")
	@Column(name = "pet_iident")
	private Long pet_iident;

	private String pet_name;
	private LocalDateTime pet_birthdate;

	@Column(name = "pet_cstatu")
	private String estado;

	private LocalDateTime audit_dfecope;
	private LocalDateTime audit_dfecre;

	@Enumerated(EnumType.STRING)
	private OBSERV audit_vobs;

	private String audit_vuser;

	public TPet() {
		// TODO Auto-generated constructor stub
	}

	public Long getPet_iident() {
		return pet_iident;
	}

	public void setPet_iident(Long pet_iident) {
		this.pet_iident = pet_iident;
	}

	public String getPet_name() {
		return pet_name;
	}

	public void setPet_name(String pet_name) {
		this.pet_name = pet_name;
	}

	public LocalDateTime getPet_birthdate() {
		return pet_birthdate;
	}

	public void setPet_birthdate(LocalDateTime pet_birthdate) {
		this.pet_birthdate = pet_birthdate;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public LocalDateTime getAudit_dfecope() {
		return audit_dfecope;
	}

	public void setAudit_dfecope(LocalDateTime audit_dfecope) {
		this.audit_dfecope = audit_dfecope;
	}

	public LocalDateTime getAudit_dfecre() {
		return audit_dfecre;
	}

	public void setAudit_dfecre(LocalDateTime audit_dfecre) {
		this.audit_dfecre = audit_dfecre;
	}

	public OBSERV getAudit_vobs() {
		return audit_vobs;
	}

	public void setAudit_vobs(OBSERV audit_vobs) {
		this.audit_vobs = audit_vobs;
	}

	public String getAudit_vuser() {
		return audit_vuser;
	}

	public void setAudit_vuser(String audit_vuser) {
		this.audit_vuser = audit_vuser;
	}

}
