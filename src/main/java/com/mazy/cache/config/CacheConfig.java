package com.mazy.cache.config;

import java.util.HashMap;
import java.util.Map;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.spring.cache.RedissonSpringCacheManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@EnableCaching
@Configuration
public class CacheConfig {

	@Value("${redis.server}")
	private String server;

	@Value("${redis.port}")
	private String port;

	// administrador de cache
	@Bean
	public CacheManager getCache(RedissonClient rClient) {
		//return new ConcurrentMapCacheManager("pets", "person"); // se va a ejecutar en memoria donde se ejecute el
																// programa, se pone los caches que se manejaran
		Map<String, CacheConfig> config = new HashMap<>();
		config.put("pets", new CacheConfig());
		return new RedissonSpringCacheManager(rClient);
	}

	//regisrando en el contexto - api cliente para conectarse a Redis
	@Bean(destroyMethod = "shutdown")
	public RedissonClient redisson() {
		Config config = new Config();
		config.useSingleServer().setAddress("redis://" + server + ":" + port);
		return Redisson.create(config);
	}

}
