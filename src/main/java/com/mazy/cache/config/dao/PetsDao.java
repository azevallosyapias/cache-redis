package com.mazy.cache.config.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mazy.cache.config.dto.TPet;

public interface PetsDao extends CrudRepository<TPet, Long>{

	List<TPet> findByEstado(String estado);
	
	@Query("SELECT t FROM TPet t where t.pet_iident = :id and t.estado= :estado") 
	Optional<TPet> findPetActive(Long id,String estado);
	
}
