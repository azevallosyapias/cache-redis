package com.mazy.cache.config.entity;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Pets implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2407448571851017788L;

	@Getter @Setter
	private Long id;
	
	@Getter @Setter
	private String name;
	
	@Getter @Setter
	private int age;
	
	@Getter @Setter
	private String address;
	
	public Pets() {
		// TODO Auto-generated constructor stub
	}
	
}
