package com.mazy.cache.service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.mazy.cache.config.dao.PetsDao;
import com.mazy.cache.config.dto.TPet;
import com.mazy.cache.config.entity.Pets;
import com.mazy.cache.util.Constants;
import com.mazy.cache.util.Constants.OBSERV;

@Service
public class PetsService {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	PetsDao petsDao;

	@CacheEvict(value = "pets" , allEntries = true)
	public Pets createPet(Pets bean) {
		TPet pet = new TPet();
		pet.setAudit_dfecope(null);
		pet.setAudit_dfecre(LocalDateTime.now());
		pet.setAudit_vobs(OBSERV.CREATE);
		pet.setEstado(Constants.STATUS.ACTIVO.getCodigo());
		pet.setPet_name(bean.getName());

		pet = petsDao.save(pet);

		bean.setId(pet.getPet_iident());

		return bean;

	}

	@Cacheable("pets") // se pone el codigo que se puso en el CacheConfig, solo entrara una vez y
						// despues solo ira a la memoria cache a traer el resultado
	public List<Pets> findAllPets() {

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		log.info("Service -> List of Pets!!!!");
		List<TPet> list = new ArrayList<TPet>();
//		Pets cuti = new Pets(1L, "Cuti", 7, "Oasis de Villa");
//		Pets peque = new Pets(2L, "Peque", 1, "Oasis de Villa");
//		Pets charlie = new Pets(3L, "Charlie", 7, "Oasis de Villa");
//		Pets risho = new Pets(4L, "Ringo", 3, "Oasis de Villa");
//
//		rpta.add(cuti);
//		rpta.add(peque);
//		rpta.add(charlie);
//		rpta.add(risho);

		list = petsDao.findByEstado(Constants.STATUS.ACTIVO.getCodigo());

		List<Pets> rpta = new ArrayList<>();
		list.stream().forEach(x -> rpta.add(new Pets(x.getPet_iident(), x.getPet_name(), 3, "Oasis")));
		return rpta;
	}

	public TPet findPetById(Long id) {

		Optional<TPet> oPet = petsDao.findPetActive(id, Constants.STATUS.ACTIVO.getCodigo());
		if (oPet.isPresent()) {
			return oPet.get();
		} else {
			return null;
		}

	}

	@CacheEvict(value = "pets" , allEntries = true)
	public void deleteById(Long idO) {
		TPet bean=findPetById(idO);
		if(null!= bean) {
			deleteById(bean);
		}else {
			
		}

	}
	
	public void deleteById(TPet bean) {
		bean.setAudit_dfecope(LocalDateTime.now());
		bean.setAudit_vobs(OBSERV.DELETE);
		bean.setEstado(Constants.STATUS.ELIMINADO.getCodigo());

		bean = petsDao.save(bean);

	}

}
